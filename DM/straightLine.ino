void straightLine (int x2, int y2)
{

  Serial.print(x2, DEC);
  Serial.print(',');
  Serial.println(y2, DEC); 

  int deltax = abs(x2 - x1);
  int deltay = abs(y2 - y1);
  int xnow = x1;
  int ynow = y1;
  int xinc1, xinc2, yinc1, yinc2, den, num, numadd, numpixels, curpixel;
  if (x2 >= x1) {	
    xinc1 = 1;
    xinc2 = 1;
  }
  else {		
    xinc1 = -1;
    xinc2 = -1;
  }
  if (y2 >= y1)		 
  {
    yinc1 = 1;
    yinc2 = 1;
  }
  else				
  {
    yinc1 = -1;
    yinc2 = -1;
  }
  if (deltax >= deltay)	  
  {
    xinc1 = 0;			
    yinc2 = 0;			
    den = deltax;
    num = deltax / 2;
    numadd = deltay;
    numpixels = deltax;
  }
  else				 
  {
    xinc2 = 0;			
    yinc1 = 0;			
    den = deltay;
    num = deltay / 2;
    numadd = deltax;
    numpixels = deltay;
  }
  for (curpixel = 0; curpixel <= numpixels; curpixel++)
  {
    Serial.print(curpixel);
    Serial.print(" / ");
    Serial.println(numpixels);
    moveTo(xnow, ynow);		 
    num += numadd;		
    if (num >= den)	
    {
      num -= den;	
      xnow += xinc1;	
      ynow += yinc1;
    }
    xnow += xinc2;		
    ynow += yinc2;
  }
  x1 = x2;
  y1 = y2;
  
 sendACK(); //#
    
    timer = 0;  
}
