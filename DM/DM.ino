//____________________________
//The Drawing Machine
//© Harvey Moon 2013
//____________________________

#include <AccelStepper.h>
//#include <stdlib.h> // atoi converter
#include <Servo.h>  //Servo Controller


#include <Ethernet.h>
#include <EthernetUdp.h>
#include <SPI.h>    

#include <OSCBundle.h>
#include <OSCBoards.h>





//-----------------
const int moveFast =486;
const int moveSlow = 54;
//-----------------
//-----PenMoves----
const int servoOff = 10;
const int servoOn = 45;
//-----------------
//int StepUnit = 112;   //use for big mxl setup
const int StepUnit = 250; // use with pololu 1/4 step
//-----------------
 int w = 110*StepUnit;
//-----------------
 int x1= 3;
 int y1 = 1;


int startX= x1;
int startY= y1;


//PINS
//-------------
const int servoPin = A2;  // backLeftCorner XLR



int a1= sqrt(pow(x1, 2)+pow(y1, 2));
int b1= sqrt(pow((w-x1), 2)+pow(y1, 2));

EthernetUDP Udp;
byte mac[] = {  
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED }; // you can find this written on the board of some Arduino Ethernets or shields

//the Arduino's IP
IPAddress ip(10, 1, 10, 252);
IPAddress outIp(10,1,10,100);
const unsigned int outPort = 6000;
//port numbers
const unsigned int inPort = 8888;
//-----------------

int widthCal = 0;
int incomingByte;
int digit;
int xval;
int yval;
int dig = 0;
byte PenOn = '-';


String SerinData;

Servo myservo; 

const int limitSwitch1 = 2;     // the number of the pushbutton pin
const int limitSwitch2 = 3;     // the number of the pushbutton pin

  boolean limit1Triggered;
  boolean limit2Triggered;

AccelStepper stepper1(1, 8, 7);
AccelStepper stepper2(1, 5, 4);


const int enablePin1 =   9;
const int enablePin2 =   6;


boolean setSpeedMode = false;
boolean setPositionMode = false;
boolean setWidth = false;

int   timer = 0;  

void setup() { 

  delay(1000); 

  Serial.begin(57600);	

  setupMotors();
  pinMode(limitSwitch1 , INPUT);
  pinMode(limitSwitch2 , INPUT);


  Serial.println("____________________________");
  Serial.println("The Drawing Machine");
  Serial.println("© Harvey Moon 2014");
  Serial.println("____________________________");

  Serial.print("True Pixel Width ");
  Serial.print(w, DEC);
  Serial.print(' ');
  Serial.println("#");

  Serial.print("Expected Starting Location ");
  Serial.print(x1, DEC);
  Serial.print(',');
  Serial.println(y1, DEC);

  Ethernet.begin(mac,ip);
  Udp.begin(inPort);
  Serial.begin(57600);
  Serial.print("starting");

}                                                                                       



void loop() {


  limit1Triggered = digitalRead(limitSwitch1);
  limit2Triggered = digitalRead(limitSwitch2);


  digitalWrite(enablePin1, LOW);
  digitalWrite(enablePin2, LOW);


  OSCMessage msgIN;
  int size;

  if( (size = Udp.parsePacket())>0)
  {
    while(size--)
      msgIN.fill(Udp.read());
    if(!msgIN.hasError())
      if(  msgIN.route("/Coord", routeCoord) ){
      }
    if(  msgIN.route("/Command", routeCommand) ){
    }
  }



  while (Serial.available() > 0) {
    dig = dig +1;
    char recieved = Serial.read();
    if(!control(recieved)){

      SerinData += recieved; 

 
       if(setPositionMode){
        if (recieved == 13 || SerinData.length()==10) {
          int xxx = SerinData.substring(0,5).toInt();
          int yyy = SerinData.substring(5).toInt();
          SerinData = ""; // Clear recieved buffer
          straightLine (xxx, yyy);
          Serial.print(xxx);
          Serial.print(", ");
          Serial.println(yyy);
          setPositionMode = false;
        }
      }

 

    }
  }

  timer++;
  if(timer >= 100000){
    //   Serial.println("#");
    timer = 0;  
  }  
}


void setupMotors(){
  pinMode(enablePin1, OUTPUT);
  digitalWrite(enablePin1, HIGH);
  pinMode(enablePin2, OUTPUT);
  digitalWrite(enablePin2, HIGH);
  // pinMode(enablePin3, OUTPUT);
  // digitalWrite(enablePin3, HIGH);

  myservo.attach(servoPin);  
  myservo.write(servoOff);
  stepper1.setMaxSpeed(moveFast);
  stepper2.setMaxSpeed(moveFast);
  stepper1.setAcceleration(10000.0);
  stepper2.setAcceleration(10000.0);
  stepper1.setCurrentPosition(a1);
  stepper2.setCurrentPosition(b1);
  delay(500);
  myservo.write(servoOff);

}


