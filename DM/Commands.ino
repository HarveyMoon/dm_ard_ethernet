void routeCommand(OSCMessage &msg, int addrOffset ){
  char boo[5];
 //char input = m
 msg.getString(0, boo, 5);
 Serial.println(boo[0]);
 control(boo[0]);
}


void routeCoord(OSCMessage &msg, int addrOffset ){
 int x = msg.getInt(0);
 int y = msg.getInt(1);
 int z = msg.getInt(2);
  
  int subMove;
  subMove = msg.match("/0", addrOffset);
  if(subMove){ 
    Serial.println("Discovered the subAddress");
  }
//Serial.print(addrOffset);
//  Serial.print("Coord");
//  Serial.print(" ");
//
//  Serial.print(x);
//  Serial.print(" ");
//  Serial.print(y);
//  Serial.print(" ");
//  Serial.println(z);
  
  if(z == 0){
   control('-');
  }
  else{
     control('+');
  }
  
straightLine(x,y);

  //sendOut();   
//  sendACK();
  //sendOut(xxx,yyy);
}

//void sendOut(){
//  Serial.println("sending");
//  OSCMessage msg("/backatcha");
//  msg.add((int32_t)abs(x-400));
//  msg.add((int32_t)y);
//  Udp.beginPacket(outIp, outPort);
//  msg.send(Udp); // send the bytes to the SLIP stream
//  Udp.endPacket(); // mark the end of the OSC Packet
//  msg.empty(); // free space occupied by message
//
//  delay(20);
//}

void sendACK(){
  Serial.println("#OK");
  OSCMessage msg("/ACK");
  msg.add("#OK");
  Udp.beginPacket(outIp, outPort);
  msg.send(Udp); // send the bytes to the SLIP stream
  Udp.endPacket(); // mark the end of the OSC Packet
  msg.empty(); // free space occupied by message
  delay(20);
}



boolean control(char Incoming) {
  switch(Incoming) {
    case 'S':
    Serial.print("The Width is : ");
    Serial.print(w, DEC);
    dig = 0;
    Serial.println("#");
    return(true);    
    break;


    case 'r':
    Serial.println("reset to Starting Point");
    straightLine(startX,startY);
    return(true);   
    break;

  



    case  '.':
    dig = 0;
    stepper1.runToNewPosition(stepper1.currentPosition()+StepUnit);
    return(true);    
    break;
    
    
    case ',':
    dig = 0;
    stepper1.runToNewPosition(stepper1.currentPosition()-StepUnit);
    return(true);    
    break;

    case '>' :
    dig = 0;
    stepper2.runToNewPosition(stepper2.currentPosition()-StepUnit); 
    return(true);    
    break;

    case '<' :
    dig = 0;
    stepper2.runToNewPosition(stepper2.currentPosition()+StepUnit);
    return(true);  
    break;


    case '*' :
    xval = 0;
    yval = 0;
    dig = 0;
    setSpeedMode = false;
    setPositionMode = true;
    setWidth = false;
    SerinData = ""; // Clear recieved buffer 
    return(true);  
    break;


    case 'z' :
    dig = 0;
    Serial.println("ZERO position reset");
    stepper1.setCurrentPosition(0);
    stepper2.setCurrentPosition(0);
    x1 = 0;
    y1 = 0;
    return(true);    
    break;

    case 'a' : 
    dig = 0;

    w = stepper1.currentPosition();

    Serial.println("Right Corner position set");

    Serial.print("w = ");
    Serial.println(w);
    x1 = w;
    y1 = 0;
    a1= sqrt(pow(x1, 2)+pow(y1, 2));
    b1= sqrt(pow((w-x1), 2)+pow(y1, 2));

    straightLine(0, 0);
    return(true);    
    break;

    case '-':
    case '+':

    if (PenOn != Incoming) {
      if (Incoming == '+') {
        delay(1000); 
        myservo.write(servoOn);
        delay(500);
        dig = 0; 
        stepper1.setMaxSpeed(moveSlow);
        stepper2.setMaxSpeed(moveSlow);
        PenOn = Incoming;
      }
      if (Incoming == '-' ) {
        delay(500);
        myservo.write(servoOff);           
        dig = 0;
        delay(250);
        stepper1.setMaxSpeed(moveFast);
        stepper2.setMaxSpeed(moveFast);
        PenOn = Incoming;
      }
    }
    return(true);
    break;

  }

  return(false);
}

